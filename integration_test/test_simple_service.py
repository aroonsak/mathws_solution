from mathWs_test import MathWsTest
from mathWs_test_utils import *

class TestSimpleService(MathWsTest):

    def getTestSuiteName(self):
        return "Test Simple Service"

    def __tc__test_simple_add_dataset_0(self):
        #Prepare
        service = "simple"
        operation = "add"
        a = [1.1, 2.2, 3.3, 4.4]
        b = [5.5, 6.6, 7.7, 8.8]
        c = [6.6, 8.8, 11.0, 13.2]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(200, "Operation completed successfully")
        expectedRes.setService(service, operation)
        expectedRes.setInput(a, b)
        expectedRes.setOutput(c)

        #Execute
        actualRes = testPostWithParams(service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_simple_add_dataset_1(self):
        #Prepare
        service = "simple"
        operation = "add"
        a = [1.1, -2.2, 3.3, 4.4]
        b = [5.5, 6.6, 7.7, -8.8]
        c = [6.6, 4.4, 11.0, -4.4]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(200, "Operation completed successfully")
        expectedRes.setService(service, operation)
        expectedRes.setInput(a, b)
        expectedRes.setOutput(c)

        #Execute
        actualRes = testPostWithParams(service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_simple_sub_dataset_0(self):
        #Prepare
        service = "simple"
        operation = "sub"
        a = [1.1, 2.2, 3.3, 4.4]
        b = [5.5, 6.6, 7.7, 8.8]
        c = [-4.4, -4.4, -4.4, -4.4]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(200, "Operation completed successfully")
        expectedRes.setService(service, operation)
        expectedRes.setInput(a, b)
        expectedRes.setOutput(c)

        #Execute
        actualRes = testPostWithParams(service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_simple_sub_dataset_1(self):
        #Prepare
        service = "simple"
        operation = "sub"
        a = [-1.1, 2.2, -3.3, 4.4]
        b = [-5.5, -6.6, 7.7, -8.8]
        c = [4.4, 8.8, -11.0, 13.2]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(200, "Operation completed successfully")
        expectedRes.setService(service, operation)
        expectedRes.setInput(a, b)
        expectedRes.setOutput(c)

        #Execute
        actualRes = testPostWithParams(service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_simple_mul_dataset_0(self):
        #Prepare
        service = "simple"
        operation = "mul"
        a = [-1.1, 2.2, -3.3, 4.4]
        b = [-5.5, -6.6, 7.7, -8.8]
        c = [6.05, -14.52, -25.41, -38.72]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(200, "Operation completed successfully")
        expectedRes.setService(service, operation)
        expectedRes.setInput(a, b)
        expectedRes.setOutput(c)

        #Execute
        actualRes = testPostWithParams(service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_simple_mul_dataset_1(self):
        #Prepare
        service = "simple"
        operation = "mul"
        a = [1, 2, 3, 4, 0]
        b = [-5.5, -6.6, 7.7, -8.8, 9.3]
        c = [-5.5, -13.2, 23.1, -35.2, 0]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(200, "Operation completed successfully")
        expectedRes.setService(service, operation)
        expectedRes.setInput(a, b)
        expectedRes.setOutput(c)

        #Execute
        actualRes = testPostWithParams(service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_simple_div_dataset_0(self):
        #Prepare
        service = "simple"
        operation = "div"
        a = [-1.1, 2.2, -3.3, 4.4]
        b = [-5.5, -6.6, 7.7, -8.8]
        c = [0.2, -0.33333333333333337, -0.42857142857142855, -0.5]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(200, "Operation completed successfully")
        expectedRes.setService(service, operation)
        expectedRes.setInput(a, b)
        expectedRes.setOutput(c)

        #Execute
        actualRes = testPostWithParams(service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))

    def __tc__test_simple_div_dataset_1(self):
        #Prepare
        service = "simple"
        operation = "div"
        a = [1, 2, 3, 4, 9.3]
        b = [-5.5, -6.6, 7.7, -8.8, 0]

        expectedRes = MathWsResponse()
        expectedRes.setHttp(400, "Divide by zero")
        expectedRes.setService(service, operation)

        #Execute
        actualRes = testPostWithParams(service, operation, a, b)

        #Verify
        assert(expectedRes.equals(actualRes))