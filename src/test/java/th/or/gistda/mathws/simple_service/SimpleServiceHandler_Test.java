package th.or.gistda.mathws.simple_service;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import th.or.gistda.mathws.base.HttpConst;
import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.base.MathWsRequest;
import th.or.gistda.mathws.base.MathWsResponse;
import th.or.gistda.mathws.service_selector.EService;
import th.or.gistda.mathws.service_selector.IServiceHandler;

public class SimpleServiceHandler_Test {

	protected IServiceHandler createSimpleServiceHandler() {
		return new SimpleServiceHandler();
	}
	
	@Test
	public void testHandle_OperationInvalid_ExpectError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceHandler simpleServ = createSimpleServiceHandler();
		
		req.setName("simple");
		req.setOperation("Invalid");
		expectedRes.setService("simple", "Invalid");
		expectedRes.setHttp(HttpConst.STATUS_BAD_REQUEST, "Invalid Simple service operation");
		
		//Execute
		simpleServ.handle(req, res);
		
		
		//Verify
		assertEquals(expectedRes, res);
		
	}
	
	@Test
	public void testHandle_OperationAdd_ExpectNoError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceHandler simpleServ = createSimpleServiceHandler();
		
		req.setName(EService.simple.getName());
		req.setOperation(ESimpleOperation.add.getName());
		req.setInputA(Arrays.asList(new Double[] {1.0, 2.0, 3.0}));
		req.setInputB(Arrays.asList(new Double[] {9.0, 8.0, 7.0}));
		expectedRes.setService(EService.simple.getName(), ESimpleOperation.add.getName());
		expectedRes.setHttp(HttpConst.STATUS_OK, "Operation completed successfully");
		expectedRes.setInput(
				Arrays.asList(new Double[] {1.0, 2.0, 3.0}), 
				Arrays.asList(new Double[] {9.0, 8.0, 7.0}));
		expectedRes.setOutput(
				Arrays.asList(new Double[] {10.0, 10.0, 10.0}));
		
		//Execute
		simpleServ.handle(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
		
	}
	
	@Test
	public void testHandle_OperationSub_ExpectNoError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceHandler simpleServ = createSimpleServiceHandler();
		
		req.setName(EService.simple.getName());
		req.setOperation(ESimpleOperation.sub.getName());
		req.setInputA(Arrays.asList(new Double[] {1.0, 8.0, 3.0}));
		req.setInputB(Arrays.asList(new Double[] {9.0, 2.0, 7.0}));
		expectedRes.setService(EService.simple.getName(), ESimpleOperation.sub.getName());
		expectedRes.setHttp(HttpConst.STATUS_OK, "Operation completed successfully");
		expectedRes.setInput(
				Arrays.asList(new Double[] {1.0, 8.0, 3.0}), 
				Arrays.asList(new Double[] {9.0, 2.0, 7.0}));
		expectedRes.setOutput(
				Arrays.asList(new Double[] {-8.0, 6.0, -4.0}));
		
		//Execute
		simpleServ.handle(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
		
	}
	
	@Test
	public void testHandle_OperationMul_ExpectNoError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceHandler simpleServ = createSimpleServiceHandler();
		
		req.setName(EService.simple.getName());
		req.setOperation(ESimpleOperation.mul.getName());
		req.setInputA(Arrays.asList(new Double[] {1.0, 8.0, 3.0}));
		req.setInputB(Arrays.asList(new Double[] {9.0, 2.0, 7.0}));
		expectedRes.setService(EService.simple.getName(), ESimpleOperation.mul.getName());
		expectedRes.setHttp(HttpConst.STATUS_OK, "Operation completed successfully");
		expectedRes.setInput(
				Arrays.asList(new Double[] {1.0, 8.0, 3.0}), 
				Arrays.asList(new Double[] {9.0, 2.0, 7.0}));
		expectedRes.setOutput(
				Arrays.asList(new Double[] {9.0, 16.0, 21.0}));
		
		//Execute
		simpleServ.handle(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
		
	}
	
	@Test
	public void testHandle_OperationDiv_ExpectNoError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceHandler simpleServ = createSimpleServiceHandler();
		
		req.setName(EService.simple.getName());
		req.setOperation(ESimpleOperation.div.getName());
		req.setInputA(Arrays.asList(new Double[] {9.0, 100.0, 1.0}));
		req.setInputB(Arrays.asList(new Double[] {3.0, 4.0, 4.0}));
		expectedRes.setService(EService.simple.getName(), ESimpleOperation.div.getName());
		expectedRes.setHttp(HttpConst.STATUS_OK, "Operation completed successfully");
		expectedRes.setInput(
				Arrays.asList(new Double[] {9.0, 100.0, 1.0}), 
				Arrays.asList(new Double[] {3.0, 4.0, 4.0}));
		expectedRes.setOutput(
				Arrays.asList(new Double[] {3.0, 25.0, 0.25}));
		
		//Execute
		simpleServ.handle(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
		
	}
	@Test
	public void testHandle_OperationDiv_ExpectDivideByZeroError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceHandler simpleServ = createSimpleServiceHandler();
		
		req.setName(EService.simple.getName());
		req.setOperation(ESimpleOperation.div.getName());
		req.setInputA(Arrays.asList(new Double[] {9.0, 100.0, 1.0}));
		req.setInputB(Arrays.asList(new Double[] {3.0, 0.0, 4.0}));
		expectedRes.setService(EService.simple.getName(), ESimpleOperation.div.getName());
		expectedRes.setHttp(HttpConst.STATUS_BAD_REQUEST, "Divide by zero");
		
		//Execute
		simpleServ.handle(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
		
	}
}
