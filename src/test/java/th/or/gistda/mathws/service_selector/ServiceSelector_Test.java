package th.or.gistda.mathws.service_selector;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import th.or.gistda.mathws.base.HttpConst;
import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.base.MathWsRequest;
import th.or.gistda.mathws.base.MathWsResponse;
import th.or.gistda.mathws.convolution_service.ConvolutionServiceHandler;
import th.or.gistda.mathws.correlation_service.CorrelationServiceHandler;
import th.or.gistda.mathws.simple_service.ESimpleOperation;
import th.or.gistda.mathws.simple_service.SimpleServiceHandler;

public class ServiceSelector_Test {
	
	private static final String simpleServName = "Simple";
	private static final String simpleServDesc = "Simple service reached";
	private static final String corrServName   = "Correlation";
	private static final String corrServDesc   = "Correlation service reached";
	private static final String convServName   = "Convolution";
	private static final String convServDesc   = "Convolution service reached";

	private IServiceSelector createMockedServiceSelector() {
		
		ServiceSelector servSel   = Mockito.spy(new ServiceSelector());
		IServiceHandler simpleServ = Mockito.mock(SimpleServiceHandler.class);
		IServiceHandler corrServ   = Mockito.mock(CorrelationServiceHandler.class);
		IServiceHandler convServ   = Mockito.mock(ConvolutionServiceHandler.class);
		
		Mockito.doAnswer(new Answer() {
			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				IMathWsRequest  req = invocation.getArgument(0);
				IMathWsResponse res = invocation.getArgument(1);
				res.setService(simpleServName, simpleServDesc);
				return null;
			}
		}).when(simpleServ).handle(Mockito.any(IMathWsRequest.class), Mockito.any(IMathWsResponse.class));
		
		Mockito.doAnswer(new Answer() {
			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				IMathWsRequest  req = invocation.getArgument(0);
				IMathWsResponse res = invocation.getArgument(1);
				res.setService(corrServName, corrServDesc);
				return null;
			}
		}).when(corrServ).handle(Mockito.any(IMathWsRequest.class), Mockito.any(IMathWsResponse.class));
		
		Mockito.doAnswer(new Answer() {
			@Override
			public Object answer(InvocationOnMock invocation) throws Throwable {
				IMathWsRequest  req = invocation.getArgument(0);
				IMathWsResponse res = invocation.getArgument(1);
				res.setService(convServName, convServDesc);
				return null;
			}
		}).when(convServ).handle(Mockito.any(IMathWsRequest.class), Mockito.any(IMathWsResponse.class));
		
		Mockito.doReturn(simpleServ).when(servSel).createSimpleServiceHandler();
		Mockito.doReturn(corrServ).when(servSel).createCorrelationServiceHandler();
		Mockito.doReturn(convServ).when(servSel).createConvolutionServiceHandler();
		
		return servSel;
	}
	
	@Test
	public void testProcess_ServiceInvalid_ExpectError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceSelector servSel = createMockedServiceSelector();
		
		req.setName("Unknown_service");
		req.setOperation("Fake_operation");
		
		expectedRes.setService("Unknown_service", "Fake_operation");
		expectedRes.setHttp(HttpConst.STATUS_BAD_REQUEST, "Invalid service name");
		
		//Execute
		servSel.process(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
	}
	
	@Test
	public void testProcess_ServiceSimple_ExpectNoError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceSelector servSel = createMockedServiceSelector();
		
		req.setName(EService.simple.getName());
		req.setOperation(ESimpleOperation.add.getName());
		
		expectedRes.setService(simpleServName, simpleServDesc);
		
		//Execute
		servSel.process(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
	}
	
	@Test
	public void testProcess_ServiceCorrelation_ExpectNoError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceSelector servSel = createMockedServiceSelector();
		
		req.setName(EService.correlation.getName());
		req.setOperation("operation");
		
		expectedRes.setService(corrServName, corrServDesc);
		
		//Execute
		servSel.process(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
	}
	
	@Test
	public void testProcess_ServiceConvolution_ExpectNoError() {
		
		//Prepare
		IMathWsRequest  req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		IServiceSelector servSel = createMockedServiceSelector();
		
		req.setName(EService.convolution.getName());
		req.setOperation("operation");
		
		expectedRes.setService(convServName, convServDesc);
		
		//Execute
		servSel.process(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
	}
	
}
