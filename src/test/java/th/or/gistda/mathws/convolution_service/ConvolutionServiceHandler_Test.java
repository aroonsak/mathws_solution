package th.or.gistda.mathws.convolution_service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;
import th.or.gistda.mathws.base.MathWsRequest;
import th.or.gistda.mathws.base.MathWsResponse;
import th.or.gistda.mathws.service.ServiceHandler_Test;

public class ConvolutionServiceHandler_Test extends ServiceHandler_Test {
	
	private ConvolutionServiceHandler convServ = Mockito.spy(new ConvolutionServiceHandler());
	
	@Test
	public void testHandle_OperationNormal_ExpectNoError() {
		
		//Prepare
		IMathWsRequest req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		Mockito.doNothing().when(convServ).convolve_normal(Mockito.any(), Mockito.any(), Mockito.any());		
		
		req.setName("convolution");
		req.setOperation("normal");
		expectedRes.setService("convolution", "normal");
		expectedRes.setHttp(200, "Operation completed successfully");
		expectedRes.setInput(null, null);
		expectedRes.setOutput(null);
		
		//Execute
		convServ.handle(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
	}
	
	@Test
	public void testHandle_OperationInvalid_ExpectError() {
		
		//Prepare
		IMathWsRequest req = new MathWsRequest();
		IMathWsResponse res = new MathWsResponse();
		IMathWsResponse expectedRes = new MathWsResponse();
		Mockito.doNothing().when(convServ).convolve_normal(Mockito.any(), Mockito.any(), Mockito.any());		
		
		req.setName("convolution");
		req.setOperation("fake_operation");
		expectedRes.setService("convolution", "fake_operation");
		expectedRes.setHttp(400, "Invalid convolution service operation");
		
		//Execute
		convServ.handle(req, res);
		
		//Verify
		assertEquals(expectedRes, res);
	}

	@Test
	public void testConvolve_normal_dataset_1() {
		//Prepare
		Double arrA[]    = new Double[] {1.0, 2.0,  3.0,  4.0,  5.0};
		Double arrB[]    = new Double[] {9.0, 8.0,  7.0,  6.0,  5.0};
		Double expArrC[] = new Double[] {9.0, 26.0, 50.0, 80.0, 115.0};
				
		List<Double> inputA = Arrays.asList(arrA);
		List<Double> inputB = Arrays.asList(arrB);
		List<Double> expOutputC = Arrays.asList(expArrC);
		List<Double> outputC = new ArrayList();
				
		//Execute
		(new ConvolutionServiceHandler()).convolve_normal(inputA, inputB, outputC);
				
		//Verify
		assertTrue(verifyErrorTolerance(expOutputC, outputC));	
	}
	
	@Test
	public void testConvolve_normal_dataset_2() {
		//Prepare
		Double arrA[]    = new Double[] { 1.1,    2.3,  -3.5,    4.7,    5.9};
		Double arrB[]    = new Double[] {-9.2,    8.4,   7.6,    6.8,   -5.0};
		Double expArrC[] = new Double[] {-10.12, -11.92, 59.88, -47.68, -31.26};
				
		List<Double> inputA = Arrays.asList(arrA);
		List<Double> inputB = Arrays.asList(arrB);
		List<Double> expOutputC = Arrays.asList(expArrC);
		List<Double> outputC = new ArrayList();
				
		//Execute
		(new ConvolutionServiceHandler()).convolve_normal(inputA, inputB, outputC);
				
		//Verify
		assertTrue(verifyErrorTolerance(expOutputC, outputC));	
	}
	
}
